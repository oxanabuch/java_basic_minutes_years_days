import java.util.Scanner;

public class MinutesYearsDays {
    public static void main(String[] args) {
        System.out.print("Enter numbers of minutes: ");
        Scanner in = new Scanner(System.in);
        int minutes = in.nextInt();

        final int MINUTES_PER_HOUR = 60;
        final int HOURS_PER_DAY = 24;
        final int MINUTES_PER_DAY = MINUTES_PER_HOUR * HOURS_PER_DAY;
        final int DAYS_PER_YEAR = 365;
        final int MINUTES_PER_YEAR = MINUTES_PER_DAY * DAYS_PER_YEAR;

        int years;
        int allDays;
        int days;

        allDays = minutes / MINUTES_PER_DAY;
        years = minutes / MINUTES_PER_YEAR;
        days = allDays - (years * DAYS_PER_YEAR);

        System.out.println(minutes + " minutes - it's about " + years + " years and " + days + " days");
    }
}
